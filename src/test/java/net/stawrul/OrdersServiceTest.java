package net.stawrul;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

    @Mock
    EntityManager em;

    // Sprawdzenie czy na pewno nie można zamówić książki, której nie ma w magazynie (ale jest na co dzień w sprzedaży)
    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookNotAvailable_placeOrderThrowsOutOfStockEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(0);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    // Sprawdzenie czy puste zamówienie wyrzuca odpowiedni wyjątek
    @Test(expected = EmptyOrderException.class)
    public void whenOrdered0Products_placeOrderThrowsEmptyOrderEx() {
        // Arrange
        Order order = new Order();
        OrdersService orderService = new OrdersService(em);

        //Act
        orderService.placeOrder(order);

        //Assert - exception expected
    }

    // Sprawdzenie czy na pewno nie da się zamówić więcej niż 3 egzemplarzy książek (mogą być różne)
    @Test(expected = TooManyException.class)
    public void whenOrderedTooManyBooks_placeOrderThrowsTooManyEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(5);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    // Sprawdzenie czy w przypadku, gdy zamawiana jest książka, której generalnie nie ma w sprzedaży wyrzucany jest odpowiedni wyjątek
    @Test(expected = UnknownBookException.class)
    public void whenOrderedNullBook_placeOrderThrowsUnknownBookEx()
    {
        Order order = new Order();
        Book book = new Book();
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(null);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    // Sprawdzenie czy poprawnie zmniejszana jest ilość egzemplarzy po zamówieniu
    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmountByOne() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }


}
