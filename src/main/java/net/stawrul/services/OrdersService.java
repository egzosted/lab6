package net.stawrul.services;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.exceptions.EmptyOrderException;
import net.stawrul.services.exceptions.OutOfStockException;
import net.stawrul.services.exceptions.TooManyException;
import net.stawrul.services.exceptions.UnknownBookException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na zamówieniach.
 */
@Service
public class OrdersService extends EntityService<Order> {

    public OrdersService(EntityManager em) {

        super(em, Order.class, Order::getId);
    }

    /**
     * Pobranie wszystkich zamówień z bazy danych.
     *
     * @return lista zamówień
     */
    public List<Order> findAll() {
        return em.createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }

    /**
     * Złożenie zamówienia w sklepie.
     * <p>
     * Zamówienie jest akceptowane, jeśli wszystkie objęte nim produkty są dostępne (przynajmniej 1 sztuka). W wyniku
     * złożenia zamówienia liczba dostępnych sztuk produktów jest zmniejszana o jeden. Metoda działa w sposób
     * transakcyjny - zamówienie jest albo akceptowane w całości albo odrzucane w całości. W razie braku produktu
     * wyrzucany jest wyjątek OutOfStockException.
     *
     * @param order zamówienie do przetworzenia
     */
    @Transactional
    public void placeOrder(Order order) {
        int counter = 0;
        int products = 0;
        int maximum = 3;
        if(order.getBooks().size() > 3)
            throw new TooManyException();

        for (Book bookStub : order.getBooks()) {
            Book book = em.find(Book.class, bookStub.getId());
            if(book == null)
                throw new UnknownBookException();
            counter++;
            products += book.getAmount();
            if (book.getAmount() < 1) {
                //wyjątek z hierarchii RuntineException powoduje wycofanie transakcji (rollback)
                throw new OutOfStockException();
            } else {
                int newAmount = book.getAmount() - 1;
                book.setAmount(newAmount);
            }
        }

        // walidacja zamowienia
        // counter odpowiada za sprawdzenie czy jest przynajmniej produkt, a products czy laczna liczba ksiazek nie jest wieksza niz x
        if(counter <= 0)
            throw new EmptyOrderException();
        if(products > maximum)
            throw new TooManyException();
        //jeśli wcześniej nie został wyrzucony wyjątek, zamówienie jest zapisywane w bazie danych
        save(order);
    }
}
