package net.stawrul.services.exceptions;

/**
 * Wyjątek sygnalizujący, że przekroczono dopuszczalną ilość książęk.
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */
public class OutOfStockException extends RuntimeException {

}
