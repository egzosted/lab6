package net.stawrul.controllers;

import net.stawrul.model.Book;
import net.stawrul.services.BooksService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CONFLICT;


/**
 * Kontroler zawierający akcje związane z książkami w sklepie.
 *
 * Parametr "/books" w adnotacji @RequestMapping określa prefix dla adresów wszystkich akcji kontrolera.
 */
@RestController
@RequestMapping("/books")
public class BooksController {

    final BooksService booksService;

    public BooksController(BooksService booksService) {
        this.booksService = booksService;
    }

    /**
     * Podpunkt: Pobieranie informacji o wszystkich produktach.
     * Pobieranie listy wszystkich książek.
     *
     * Żądanie:
     * GET /books
     *
     * @return lista książek
     */
    @GetMapping
    public List<Book> listBooks() {
        return booksService.findAll();
    }

    /**
     * Podpunkt: Dodawanie nowych produktów.
     * Dodawanie nowej książki.
     *
     * Żądanie:
     * POST /books
     *
     * @param book obiekt zawierający dane nowej książki, zostanie zbudowany na podstawie danych
     *             przesłanych w ciele żądania (automatyczne mapowanie z formatu JSON na obiekt
     *             klasy Book)
     * @param uriBuilder pomocniczy obiekt do budowania adresu wskazującego na nowo dodaną książkę,
     *                   zostanie wstrzyknięty przez framework Spring
     *
     * @return odpowiedź HTTP dla klienta
     */
    @PostMapping
    public ResponseEntity<String> addBook(@RequestBody Book book, UriComponentsBuilder uriBuilder) {

        if (booksService.find(book.getId()) == null) {
            booksService.save(book);
            URI location = uriBuilder.path("/books/{id}").buildAndExpand(book.getId()).toUri();
            return ResponseEntity.created(location).build();

        } else {
            return ResponseEntity.status(CONFLICT).build();
        }
    }

    /**
     * Podpunkt: Aktualizacja danych produktu.
     * Aktualizacja danych książki.
     *
     * Żądanie:
     * PUT /books/{id}
     *
     * @param book
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<Void> updateBook(@RequestBody Book book) {
        if (booksService.find(book.getId()) != null) {
            booksService.save(book);
            return ResponseEntity.ok().build();

        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    /**
     * Podpunkt: Aktualizacja danych produktu.
     * Usunięcie książki z księgarni (wszystkich jej sztuk)
     *
     * Żądanie:
     * DELETE /books/{id}
     *
     * @param book
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBook(@RequestBody Book book) {
        if (booksService.find(book.getId()) != null) {
            booksService.delete(book);
            return ResponseEntity.ok().build();

        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
